function dec = merge_decompositions(decompositions)
  n = length(decompositions);
  if (n == 0)
     dec = cell(0);
     return;
  endif

  dec = decompositions{1};
  decompositions(1) = [];

  num = 1;
  for i=1:n-1
    dec1 = dec;
    dec = cell(0);
    dec2 = decompositions{i};

    while (length(dec1) > 0)
      tile1 = dec1{1};
      zono1 = tile1{1};
      j = 1;
      while (j <= length(dec2))
        tile2 = dec2{j};
	zono2 = tile2{1};
        is1in2 = inclusion_zonotope(zono1, zono2);
	is2in1 = inclusion_zonotope(zono2, zono1);

	# printf("Z%i <-> Z%i ? %i/%i\n", tile1{3}, tile2{3}, is1in2, is2in1);
        
	if (is1in2 && is2in1)

	   # equal zones -> combine and drop both
	   tile = merge_tiles(tile1, tile2, zono1, num);
	   dec{length(dec)+1} = tile;
	   num = num+1;
	   dec2(j) = [];	   
	   break;
	elseif (is2in1)

	   # tile2 is a subtile -> combine and drop tile2
	   tile = merge_tiles(tile1, tile2, zono2, num);
	   dec{length(dec)+1} = tile;
	   num = num+1;
	   dec2(j) = [];
	   continue;
	elseif (is1in2)

	   # tile1 is a subtile -> combine and drop tile1
           tile = merge_tiles(tile1, tile2, zono1, num);
	   dec{length(dec)+1} = tile;
	   num = num+1;
           break;
	endif

        # tiles are incompatible, try the next one
        j = j+1;
      endwhile
 
      # drop current tile of dec1
      dec1(1) = [];
    endwhile
  endfor
endfunction
