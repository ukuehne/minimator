function P = map_poly(P, pattern, fps)
  Q = [];
  for i=1:length(pattern)
    Q = [];
    m = pattern(i);
    for j=1:length(P)
      V = P(:,j);
      V = next_X(V, m);
      Q = [Q, V];
    endfor
    P = Q;
    plot_poly(fps(m), P);
  endfor  
endfunction
