function void = store_tile(zonotope, pattern, num, controllable)
  global decomposition;
  
  tile = {zonotope; pattern; num; controllable};
  decomposition{length(decomposition)+1} = tile;

  void = 0;
endfunction
