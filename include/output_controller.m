function output_controller(zonotope,pattern,tile_num)
	global controller_file
	global example_name
	global already_controller
		fprintf(controller_file,"\t\telseif is_in_zonotope(X,");
		fprintf(controller_file,string_zonotope(zonotope));
		fprintf(controller_file,")\n");
# 		fprintf(controller_file,"\t\t\tprintf(\"%d\");\n",tile_num);
	for i=1:length(pattern)
		fprintf(controller_file,"\t\t\tX=next_X(X,%d);\n",pattern(i));
		if i == length(pattern)
			fprintf(controller_file,"\t\t\thistory_X = [history_X,X];\n");
		endif
	endfor
endfunction
