function void = dump_decomposition(decomposition, player)
  global outpath;
  global example_name;

  # dump decomposition file for later post image computation
  if (player > 0)
    filename = strcat(outpath, example_name, int2str(player), ".dec");
  else
    filename = strcat(outpath, example_name, ".dec");
  endif
  file = fopen(filename, "w");
  fprintf(file, "Decomposition{\n");

  for i=1:length(decomposition)
    tile = decomposition{i};
    zonotope = tile{1};
    pattern = tile{2};
    num = tile{3};
    
    fprintf(file, "\tTile{\n");
    fprintf(file, "\t\tnumber = \"%d\";\n", num);
    fprintf(file, "\t\tpattern = \"%s\";\n", list2str(pattern, "%i"));
    fprintf(file, "\t\tV = \"");
    corners = extreme_points(zonotope);
    for i=1:length(corners(:,1))-1
      fprintf(file, "(%f,%f),", min(corners(i,:)), max(corners(i,:)));
    endfor
    i = length(corners(:,1));
    fprintf(file, "(%f,%f)", min(corners(i,:)), max(corners(i,:)));
    fprintf(file, "\";\n");

    if (length(tile) > 4)
      alpha_p = tile{5};
      fprintf(file, "\t\talpha = \"");
      for i=1:length(alpha_p(:,1))-1
	fprintf(file, "%f ",alpha_p(i,:))
	fprintf(file, ";");
      endfor
      i = length(alpha_p(:,1));
      fprintf(file, "%f ",alpha_p(i,:))
      fprintf(file, "\";\n");
      
      beta_p = tile{6};
      fprintf(file, "\t\tbeta = \"");
      fprintf(file, "%f ",beta_p)
      fprintf(file, "\";\n");
    endif
    fprintf(file, "\t}\n");
  endfor

  fprintf(file, "}\n");
  fclose(file);

  # dump decomposition figure and graph script
  sh = fopen("plot.sh", "w");
  fprintf(sh, "graph -T svg -C -X X -Y Y ");
  for i=1:length(decomposition)
    tile = decomposition{i};
    num = tile{3};
    filename = strcat(outpath, "plot/", "zone_", int2str(player), "_", int2str(num));
    zonotope = tile{1};
    dot_zonotope(filename, zonotope);    
    
    if (tile{4})
#      col = length(tile{2});
      col = 2 + mod(tile{3}*7, 11);
      fprintf(sh, " -m 2 -q %f ", 0.08*col);
    else
      fprintf(sh, " -m 1 -q 0.2 ");
    endif
    fprintf(sh, filename);
  endfor
  svg = strcat(outpath, example_name, int2str(player), ".svg");
  fprintf(sh, " > %s\n", svg);
  fclose(sh);
  system(strcat("sh ./plot.sh"));

  for i=1:length(decomposition)
    tile = decomposition{i};
    Z = tile{1};
    num = tile{3};
    col = (4 + mod(tile{3}*11, 7))*0.1;
    rectangle("Position", [Z(1,1)-Z(1,2), Z(2,1)-Z(2,3), 2*Z(1,2), 2*Z(2,3)],
	      "EdgeColor", "black",
	      "FaceColor", col*[0.2,1.0,0.2]);
#    text(Z(1,1), Z(2,1), int2str(num));
#    label = sprintf("#%i\n<%s>", num, list2str(tile{2}, "%i"));
    label = sprintf("<%s>", list2str(tile{2}, "%i"));
    text(Z(1,1), Z(2,1), label, 'horizontalalignment', 'center', "fontsize", 14);
  endfor
  filename = strcat(outpath, example_name, int2str(player), "-dec.jpg");
  print(filename, "-djpg");
endfunction
