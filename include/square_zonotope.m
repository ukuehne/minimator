function squared_zonotope = square_zonotope(zonotope)
	[C,G] = zonotope_decompo(zonotope);
	n=length(G(1,:));
	m=length(G(:,1));
	G_squared = zeros(m,m);
	for i=1:m
		for j=1:n
			G_squared(i,i)=G_squared(i,i) + abs(G(i,j));
		endfor
	endfor
	squared_zonotope=[C,G_squared];
endfunction
