function end_controller()
	global example_name
	global controller_file
	fprintf(controller_file,"\tendif\n");
	fprintf(controller_file,"endfor\n");
	fprintf(controller_file,"plot(history_X(1,:),history_X(2,:),\"-@11\");\n")
	fprintf(controller_file,strcat("print(\"",example_name,"/",example_name,"_plot.png\",\"-dpng\");\n"));
endfunction