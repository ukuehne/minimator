function s = matrix2str(m, f)
  s = "[";
  for i=1:length(m(:,1))
    l = m(i,:);
    s = strcat(s, list2str(l, f));
    if (i == length(m(:,1))) 
      s = strcat(s, "]");
    else
      s = strcat(s, "; ");
    endif
  endfor
endfunction
