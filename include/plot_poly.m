function plot_poly(f, P)
  for i=1:length(P)
    x = P(:,i);
    fprintf(f, "%f %f\n", x(1), x(2));
  endfor
  fprintf(f, "\n");
endfunction
