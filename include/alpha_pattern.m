function alpha_p = alpha_pattern(pattern,zonotope)
	n = length(pattern(1,:));
	if n == 1
		alpha_p = alpha_c(pattern(:,1),zonotope);
	else 
	  mode = pattern(:,n);
	  rest = pattern(:,1:n-1);
	  zimg = affine_transfo(zonotope, alpha_c(mode,zonotope), beta_c(mode, zonotope));
	  alpha_p = alpha_c(mode,zonotope) * alpha_pattern(rest, zimg);
	endif
endfunction
