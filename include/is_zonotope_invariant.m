function flag = is_zonotope_invariant(zonotope_control,zonotope_target,pattern,player)
  global zonotope_safety;
  global option_safety;
  global numberPlayers;
  global numberModes;

  flag_safe = 1;

  # adversary != player
  # FIXME: works only for 2 players
  advers = 2;
  if (player == 2)
     advers = 1;
  endif

  # set of current images
  imgs = {zonotope_control};

  for i=1:length(pattern)
    next_imgs = cell(0);

    # post images of current image set
    for j=1:length(imgs)
      modes = {zeros(1, numberPlayers)};
 
      # one image for each possible mode combination of adversaries
      for p=1:numberPlayers
	next_modes = cell(0);

	for m=1:length(modes)
	  mode = modes{m};
	  if (p == player)
	   
	    # we are controlling the mode
	    mode(p) = pattern(i);
	    next_modes{end+1} = mode;
	  else
	    
            # try each possible mode for this player  
	    for k=1:numberModes(p)
              mode(p) = k;
              next_modes{end+1} = mode;		
	    endfor
	  endif    
	endfor
	modes = next_modes;
      endfor

      # build post images
      for k=1:length(modes)
	mode = modes{k};
	img = affine_transfo(imgs{j},
			     alpha_c(mode), 
			     beta_c(mode));	
	next_imgs{end+1} = img;
      endfor
    endfor

    imgs = next_imgs;
    
  ## if (option_safety)
  ##   if (!inclusion_zonotope(zonotope_control, zonotope_safety))
  ## 	flag_safe = 0;
  ##   endif
  ## endif
    
  endfor

  # Test inclusion of all images in target zone
  flag = 1;
  
  dim = length(zonotope_control(:,1));
  if (exist('objective_c'))
    A = objective_c(player);
  else
    A = eye(dim);
  endif
  B = [0;0];
  projected_target = affine_transfo(zonotope_target, A, B);
  for i=1:length(imgs)
    projected_img = affine_transfo(imgs{i}, A, B);
    flag = flag && inclusion_zonotope(projected_img, projected_target);
  endfor
endfunction;
