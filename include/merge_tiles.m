function tile = merge_tiles(tile1, tile2, zono, num)
  tile = cell(0);

  tile{1} = zono;
  tile{3} = num;

  # compute combined pattern
  pat1 = tile1{2};
  pat2 = tile2{2};
  l1 = length(pat1);
  l2 = length(pat2);
  
  pat = [];
  if (l1 > 0 && l2 > 0)
    l = lcm(l1, l2);
    pat = zeros(2, l);
    
    i = 1;
    for n=1:(l/l1)
      for j=1:length(pat1)
	pat(1, i) = pat1(j);
	i = i+1;
      endfor
    endfor
    
    i = 1;
    for n=1:(l/l2)
      for j=1:length(pat2)
	pat(2, i) = pat2(j);
	i = i+1;
      endfor
    endfor
  endif  
  tile{2} = pat;

  # controllable?
  tile{4} = tile1{4} && tile2{4};
endfunction
