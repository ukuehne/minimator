function [R,MaxDepth,MaxLength,flags,tau,xName,yName] = parse_args(args)
        global option_safety = 0;
	global zonotope_safety = [];

	argc = length(args);
	R = [];
	flag_R = 0;
	MaxDepth = 3;
	flag_M = 0;
	MaxLength = 5;
# 	Max_L = 0;
	flags = [0];
	tau = 1;
	xName = "X";
	yName = "Y";
	for i=1:2:argc
		if (strcmp(args(i),"-unfolding") == 1)
			flags(1) = str2num(args(i+1){1});
		endif
		if (strcmp(args(i),"-MaxDepth") == 1)||(strcmp(args(i),"-d") == 1)
			MaxDepth = str2num(args(i+1){1});
		elseif (strcmp(args(i),"-MaxLength") == 1)||(strcmp(args(i),"-n") == 1)
			MaxLength = str2num(args(i+1){1});
		elseif (strcmp(args(i),"-R") == 1)
			R = str2zon(args(i+1){1});
		elseif (strcmp(args(i),"-S") == 1)		  
			zonotope_safety = str2zon(args(i+1){1});
			option_safety = 1;
		elseif (strcmp(args(i),"-tau") == 1)||(strcmp(args(i),"-h") == 1)||(strcmp(args(i),"-t") == 1)
			tau = str2num(args(i+1){1});
		elseif (strcmp(args(i),"-X") == 1)
			xName = args(i+1){1};
		elseif (strcmp(args(i),"-Y") == 1)
			yName = args(i+1){1};
		endif
	endfor
endfunction
