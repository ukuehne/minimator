function next_pattern = next_pattern(pattern)
	global numberModes;
	next_pattern = pattern;
	n=length(pattern);
	i = n;
	while (i > 0 && next_pattern(i) == numberModes)
		next_pattern(i) = 1;
		i = i-1;
	endwhile
	if (i == 0)
	 next_pattern = [1,next_pattern];
	else next_pattern(i)=next_pattern(i) + 1;
	endif
	return
endfunction
