function extreme_points = extreme_points(zonotope_e)
	[C,G]=zonotope_decompo(zonotope_e);
	n = length(G(1,:));
	extreme_points = [];
	for i=0:2^n-1
		extreme_points = [extreme_points,C+G*int2bin(i,n,-1)];
	endfor
endfunction
