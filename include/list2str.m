function s = list2str(l,f)
  s = "";
  for i=1:length(l)
    v = sprintf(f, l(i));
    if (i < length(l))
      s = strcat(s, v, ", ");
    else
      s = strcat(s, v);
    endif
  endfor
endfunction
