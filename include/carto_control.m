function is_contr = carto_control(zonotope_to_control, zonotope_target, depth,tile_num,player)
	global MaxDepth;
	global MaxLength;

	if tile_num >= 0
	  is_contr = is_controllable(zonotope_to_control,zonotope_target,tile_num,player);
	else 
	  is_contr = 0;
	endif
	if (is_contr >= 1)
	   # done!
	else if (depth >= MaxDepth) 
		
	       # Maximum depth reached, store uncontrollable tile
	       store_tile(zonotope_to_control, [], tile_num, 0);
	       printf("Tile #%d \tuncontrollable\n",tile_num);      
	     else 	

	       # Subdivise and recursive call
	       dim = length(zonotope_to_control(:,1));
	       printf("Tile #%d \tsubdivised into tiles #%d #%d #%d #%d... \n",tile_num,(2^dim)*tile_num+1,(2^dim)*tile_num+2,(2^dim)*tile_num+3,(2^dim)*tile_num+4);
	       is_contr = 1;
	       for i=0:(2^dim)-1
		 sub_contr = carto_control(split_zonotope(zonotope_to_control,i),zonotope_target,depth + 1,(2^dim)*tile_num + i+1,player);
		 is_contr = is_contr && sub_contr;
	       endfor
	     endif
	endif
endfunction
