function is_contr = main_control()
  global example_name;
  global total_pattern_tried;
  global MaxDepth;
  global file_to_carto;
  global MaxLength;
  global tau;
  global flags;
  [zonotope_control,MaxDepth,MaxLength,flags,tau,xName,yName] = parse_args(argv());

  # prepare directories for output
  global outpath = strcat("./",example_name,"/");
  mkdir (outpath);
  mkdir (strcat(outpath, "plot/"));

  global decomposition;                # current decomposition
  global decompositions = cell(0);     # global list of decompositions
  global invariant = zonotope_control; # rectangle R
 
  # Start controller synthesis
  global numberModes;
  global numberPlayers = length(numberModes);
  tic();
  for player=1:numberPlayers
    decomposition = cell(0);
      
    fprintf("================================================\n");
    fprintf("PLAYER %i\n", player);
    fprintf("================================================\n");

    # compute controller
    is_contr = carto_control(zonotope_control, zonotope_control, 0,0,player);

#    if (is_contr)
    dump_decomposition(decomposition, player);
    decompositions{player} = decomposition;
#    endif
  endfor    
  elapsed_time = toc();
  fprintf("================================================\n");

  # generate output 
  printf("Computation time: %f seconds | Number of patterns tried: %d\n",elapsed_time,total_pattern_tried);

  printf("Merging decompositions...\n");
  dec = merge_decompositions(decompositions);

  # compute combined affine mappings for patterns
  for i=1:length(dec)
    tile = dec{i};
    if (tile{4})
      zono = tile{1};
      pat = tile{2};
      alpha = alpha_pattern(pat, zono);
      beta  = beta_pattern(pat, zono);      
      tile{5} = alpha;
      tile{6} = beta;
    endif
    dec{i} = tile;
  endfor

  printf("Writing merged decomposition...\n");
  dump_decomposition(dec, 0);

  printf("Writing controllers...\n");
  controller = dump_controller(decompositions);
  if (is_contr)
    printf("Start simulation...\n");
    source(controller);
  endif
endfunction
