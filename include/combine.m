function c = combine(x, ys)
  if (length(ys) == 0)
    c = cell(0);
    for i=1:x
      l = [i];
      c{end+1} = l;
    endfor
    return;
  else
    tail = combine(ys(1), ys(2:end));
    c = cell(0);
    for i=1:length(tail)
      l = tail{i};
      for j=1:x
	jl = [j, l];
	c{end+1} = jl;
      endfor
    endfor    
  endif
endfunction
