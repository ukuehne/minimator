function output_dec(zonotope,pattern,tile_num)
	global post_delta_file;
	fprintf(post_delta_file,"\tTile{\n");
	fprintf(post_delta_file,"\t\tnumber = \"%d\";\n",tile_num);
	fprintf(post_delta_file,"\t\tV = \"");
	corners = extreme_points(zonotope);
	for i=1:length(corners(:,1))-1
		fprintf(post_delta_file,"(%f,%f),",min(corners(i,:)),max(corners(i,:)))
	endfor
	i = length(corners(:,1));
	fprintf(post_delta_file,"(%f,%f)",min(corners(i,:)),max(corners(i,:)))
	fprintf(post_delta_file,"\";\n");

	alpha_p = alpha_pattern(pattern,zonotope);
	beta_p = beta_pattern(pattern,zonotope);

	fprintf(post_delta_file,"\t\talpha = \"");
	for i=1:length(alpha_p(:,1))-1
		fprintf(post_delta_file,"%f ",alpha_p(i,:))
		fprintf(post_delta_file,";");
	endfor

	i = length(alpha_p(:,1));
	fprintf(post_delta_file,"%f ",alpha_p(i,:))
	fprintf(post_delta_file,"\";\n");
	
	fprintf(post_delta_file,"\t\tbeta = \"");
	fprintf(post_delta_file,"%f ",beta_p)
	fprintf(post_delta_file,"\";\n\t}\n");
endfunction
