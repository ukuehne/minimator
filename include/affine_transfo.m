function zonotope_image = affine_transfo(zonotope,alpha_c,beta_c)
  global perturbed;
  if (perturbed != 0) 
    global zonotope_perturbations;
    [C,G]=zonotope_decompo(zonotope);
    [C_eps,G_eps] = zonotope_decompo(zonotope_perturbations);
    zonotope_image = [alpha_c*C + beta_c,alpha_c*G,(alpha_c - eye(2))*G_eps];
  else
    [C,G]=zonotope_decompo(zonotope);
    zonotope_image = [alpha_c*C + beta_c,alpha_c*G];
  endif
endfunction
