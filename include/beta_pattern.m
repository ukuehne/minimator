function beta_p = beta_pattern(pattern,zonotope)
	n = length(pattern(1,:));
	if n == 1
		beta_p = beta_c(pattern(:,1),zonotope);
	else 
	  mode = pattern(:,n);
          rest = pattern(:,1:n-1);
	  zimg = affine_transfo(zonotope, alpha_c(mode,zonotope), beta_c(mode,zonotope));
	  beta_p = beta_c(mode,zonotope) + alpha_c(mode,zonotope) * beta_pattern(rest, zimg);
	endif
endfunction
