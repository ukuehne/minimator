function X = random_point(zono)
  X = zono(:,1);
  for i=2:length(zono)
    a = (2*rand) - 1.0;
    X = X + a*zono(:,i);
  endfor
endfunction
