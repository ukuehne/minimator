function P = make_poly(Z)
  V = extreme_points(Z);
  P = [V(:,1), V(:,2), V(:,4), V(:,3), V(:,1)];
endfunction
