function plot_zonotope = plot_zonotope(zonotope)
	[C,G] = zonotope_decompo(zonotope);
	extreme_points = [C+G*[-1;-1],C+G*[1;-1],C+G*[1;1],C+G*[-1;1],C+G*[-1;-1]];
	if length(C) > 2
		printf("Warning: dot_zonotope requires the case study to be in 2 dimensions.\nYour case study appears to be in %d dimensions\n",length(C));
	else
		plot(extreme_points(1,:),extreme_points(2,:));
	endif
endfunction
