function vec_binaire = int2bin(k,m,l)
	vec_binaire = zeros(m,1);
	for i=1:m
		vec_binaire(i,1)=l;
	endfor
	i=m;
	while(k~=0)
		if (mod(k,2)==1)
			vec_binaire(i,1)=1;
			k=(k-1)/2;
		else k=k/2;
		endif
		i--;
	endwhile

endfunction
