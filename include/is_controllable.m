function is_contr = is_controllable(zonotope_to_control,zonotope_target,tile_num,player)
  global MaxLength
  global total_pattern_tried;
  pattern = [1];
  admissible_pattern_found = 0;
  is_contr = 0;

  current_length=0;
  while ((length(pattern) <= MaxLength) && (admissible_pattern_found == 0))
    total_pattern_tried = total_pattern_tried + 1;
    if (is_zonotope_invariant(zonotope_to_control,zonotope_target,pattern,player) == 1)
      admissible_pattern_found = 1;
      printf("Tile #%d \tcontrollable by pattern ",tile_num);
      for i=1:length(pattern)
	printf("%d ",pattern(i));
      endfor
      printf("\n");
      store_tile(zonotope_to_control, pattern, tile_num, 1);
    endif
    pattern = next_pattern(pattern);		
  endwhile
  is_contr = admissible_pattern_found;
endfunction
