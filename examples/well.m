#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

########################
#    MODEL CONSTANTS   #
########################

global example_name = "well";
global numberModes = 1;

function A = alpha_c(mode)
  global tau;
  if (mode == 1)
    A = expm([-1,3; 0,-3]*tau);
  endif
endfunction 

function B = beta_c(mode)
  B = [0;0];
endfunction 

# Start control synthesis
is_contr = main_control();
if (is_contr)
  exit(0)
else
  exit(1)
endif

