#!/bin/sh

export BENCH="spiral"

export MAXLENGTH=5
export MAXDEPTH=3
export R="[-1,1.2]*[-1,1.2]"
export TAU=0.2

export POST=20
export HULL=-1
export BBOX=-1

./run.sh
