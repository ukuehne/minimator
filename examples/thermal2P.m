#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

##########################
# CONSTANTS OF THE MODEL #
##########################

global example_name = "thermal2P";
global numberModes = [2,2];

global a_12 = 0.05;
global a_21 = 0.05;
global a_e1 = 0.005;
global a_e2 = 0.0033;
global a_f1 =  0.0083;
global a_f2 =  0.0083;

global T_e = 10;
global T_f = 50;

global A_1 = [ 	-a_21-a_e1 	, a_21;
		a_12		, -a_12 - a_e2] ;

global A_2 = [ 	-a_21-a_e1-a_f1	, a_21;
		a_12		, -a_12-a_e2] ;

global A_3 = [ 	-a_21-a_e1	, a_21;
		a_12		, -a_12-a_e2-a_f2] ;

global A_4 = [ 	-a_21-a_e1-a_f1	, a_21;
		a_12		, -a_12-a_e2-a_f2] ;

global B_1 = [a_e1*T_e            ; a_e2*T_e] ;            # off off
global B_2 = [a_e1*T_e + a_f1*T_f ; a_e2*T_e] ;            # on  off
global B_3 = [a_e1*T_e            ; a_e2*T_e + a_f2*T_f] ; # off on
global B_4 = [a_e1*T_e + a_f1*T_f ; a_e2*T_e + a_f2*T_f] ; # on  on


function alpha_r = alpha_c(modes)
  mode1 = modes(1);
  mode2 = modes(2);

  global A_1;
  global A_2;
  global A_3;
  global A_4;
  
  global tau;
	
  persistent A11 = expm(A_1*tau);
  persistent A21 = expm(A_2*tau);
  persistent A12 = expm(A_3*tau);
  persistent A22 = expm(A_4*tau);
  
  if (mode1 == 1 && mode2 == 1)
    alpha_r = A11; 
  elseif (mode1 == 2 && mode2 == 1)
    alpha_r = A21;
  elseif (mode1 == 1 && mode2 == 2)
    alpha_r = A12;
  elseif (mode1 == 2 && mode2 == 2)
    alpha_r = A22;
  endif
endfunction


function beta_r = beta_c(modes)
  mode1 = modes(1);
  mode2 = modes(2);

  global A_1;
  global A_2;
  global A_3;
  global A_4;
  global B_1;
  global B_2;
  global B_3;
  global B_4;
  
  global tau;
  
  persistent B11 = inv(A_1) * (expm(A_1*tau)-eye(2)) * B_1;
  persistent B21 = inv(A_2) * (expm(A_2*tau)-eye(2)) * B_2;
  persistent B12 = inv(A_3) * (expm(A_3*tau)-eye(2)) * B_3;
  persistent B22 = inv(A_4) * (expm(A_4*tau)-eye(2)) * B_4;
  
  if (mode1 == 1 && mode2 == 1)
    beta_r = B11; 
  elseif (mode1 == 2 && mode2 == 1)
	  beta_r = B21;
  elseif (mode1 == 1 && mode2 == 2)
    beta_r = B12;
  elseif (mode1 == 2 && mode2 == 2)
    beta_r = B22;
  endif
endfunction

function P = objective_c(player)
  if (player == 1)
    P = [1,0;0,0];
  elseif (player == 2)
    P = [0,0;0,1];
  endif
endfunction

# Start control synthesis
is_contr = main_control()

if (is_contr == 0) 
  exit(1)
else
  exit(0)
endif
