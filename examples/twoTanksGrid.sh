#!/bin/sh

export BENCH="twoTanksGrid"

export MAXLENGTH=4
export MAXDEPTH=5
export R="[-1.5,2.5]*[-0.5,1.5]"
export TAU=0.2

#export POST=15
#export HULL=-1
#export BBOX=-1

LIBDIR=`pwd`/../lib
export LD_LIBRARY_PATH=$LIBDIR:$LD_LIBRARY_PATH

# Compute decomposition 
octave -p $MINIMATOR/include $BENCH.m -MaxLength $MAXLENGTH -MaxDepth $MAXDEPTH -R $R -tau $TAU

