#!/bin/sh

export BENCH="boost"

export MAXLENGTH=10
export MAXDEPTH=4
export R="[1.75,1.95]*[1.14,1.26]"
export S="[1.7,2.0]*[1.1,1.3]"
export TAU=0.5

export POST=10
export HULL=10
export BBOX=20
export MINSIMP=20  # do not simplify if less than 20 tiles

./run.sh
