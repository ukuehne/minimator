#!/bin/sh

export BENCH="thermal2P"

export MAXLENGTH=5
export MAXDEPTH=3
export R="[20.25,21.75]*[20.25,21.75]"
export S="[20,22]*[20,22]"
export TAU=5

export POST=10
export HULL=-1
export BBOX=-1

./run.sh
