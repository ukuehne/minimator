#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

##########################
# CONSTANTS OF THE MODEL #
##########################
global example_name = "thermal";
global numberModes = 2;

global a_12 = 0.05;
global a_21 = 0.05;
global a_e1 = 0.005;
global a_e2 = 0.0033;
global a_f =  0.0083;

global T_e = 10;
global T_f = 50;

global A_1 = [ 	-a_21-a_e1 	, a_21;
		a_12		, -a_12 - a_e2] ;

global A_2 = [ 	-a_21-a_e1-a_f	, a_21;
		a_12		, -a_12-a_e2] ;

global B_1 = [a_e1*T_e           ; a_e2*T_e] ;
global B_2 = [a_e1*T_e + a_f*T_f ; a_e2*T_e] ;


function alpha_r = alpha_c(mode)
	global A_1;
	global A_2;
	global A_3;

	global tau;

	if (mode == 1)
		alpha_r = expm(A_1*tau);
	elseif (mode == 2)
		alpha_r = expm(A_2*tau);
	endif
endfunction

function beta_r = beta_c(mode)
	global A_1;
	global A_2;
	global A_3;
	global B_1;
	global B_2;
	global B_3;

	global tau;

	if (mode == 1)
		beta_r = inv(A_1) * (expm(A_1*tau)-eye(2)) * B_1;
	elseif (mode == 2)
		beta_r = inv(A_2) * (expm(A_2*tau)-eye(2)) * B_2;
	endif
endfunction

# Start control synthesis
is_contr = main_control()

if (is_contr == 0) 
  exit(1)
else
  exit(0)
endif
