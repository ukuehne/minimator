#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

########################
#    MODEL CONSTANTS   #
########################

global example_name = "twoTanks2P";
global numberPlayers = 2;
global numberModes = [2,2];

# Projection function to evaluate the different objective functions of
# the players
function P = objective_c(player)
  if (player == 1)
    P = [1, 0; 0, 0];
  elseif (player == 2)
    P = [0, 0; 0, 1];
  endif	 
endfunction

function A = alpha_c(modes)
  mode1 = modes(1);
  mode2 = modes(2);

  global tau;
  persistent A1 = [exp(-tau), 0; -exp(-tau)+1,1];
  persistent A2 = expm([-1,0;1,-1]*tau);

  if (mode1 == 1)
    A = A1;
  elseif (mode1 == 2)
    A = A2;
  else
    disp("ERROR");
    exit(-1);    
  endif
endfunction 

function B = beta_c(modes)
  mode1 = modes(1);
  mode2 = modes(2);

  global tau;  
  persistent B1 = 2 * [exp(-tau) - 1;1-exp(-tau)-tau];
  persistent B2 = -3 * [exp(-tau) - 1;1-exp(-tau)-tau];
  persistent B3 = inv([-1,0;1,-1]) * (expm([-1,0;1,-1]*tau) - eye(2)) * [3;0];
  persistent B4 = inv([-1,0;1,-1]) * (expm([-1,0;1,-1]*tau) - eye(2)) * [3;-5];

  if (mode1 == 1 && mode2 == 1)
    B = B1;
  elseif (mode1 == 1 && mode2 == 2)
    B = B2; 
  elseif (mode1 == 2 && mode2 == 1)
    B = B3; 
  elseif (mode1 == 2 && mode2 == 2)
    B = B4; 
  else
    disp("ERROR");
    exit(-1);
  endif
endfunction 

# Start control synthesis
is_contr = main_control();
if (is_contr)
  exit(0)
else
  exit(1)
endif

