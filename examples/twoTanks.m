#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

########################
#    MODEL CONSTANTS   #
########################

global example_name = "twoTanks";
global numberModes = 4;

function A = alpha_c(mode)
	global tau;
	if (mode == 1)
		A = [exp(-tau), 0; -exp(-tau)+1,1];
	elseif (mode == 2)
		A = [exp(-tau), 0; -exp(-tau)+1,1];
	elseif (mode == 3)
		A = expm([-1,0;1,-1]*tau);
	elseif (mode == 4)
		A = expm([-1,0;1,-1]*tau);
	endif
endfunction 

function B = beta_c(mode)
	global tau;
	if (mode == 1)
		B = 2 * [exp(-tau) - 1;1-exp(-tau)-tau];
	elseif (mode == 2)
		B = -3 * [exp(-tau) - 1;1-exp(-tau)-tau];
	elseif (mode == 3)
#		B = (expm([-1,0;1,-1]*tau) - eye(2))*inv([-1,0;1,-1])*[3;0];
		B = inv([-1,0;1,-1]) * (expm([-1,0;1,-1]*tau) - eye(2)) * [3;0];
	elseif (mode == 4)
#		B = (expm([-1,0;1,-1]*tau) - eye(2))*inv([-1,0;1,-1])*[3;-5];
		B = inv([-1,0;1,-1]) * (expm([-1,0;1,-1]*tau) - eye(2)) * [3;-5];
	endif
endfunction 

# Start control synthesis
is_contr = main_control();
if (is_contr)
  exit(0)
else
  exit(1)
endif

