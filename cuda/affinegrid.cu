#include <stdio.h>

__device__ int rbase(int n, int d, int x, int y){
  int rsize = d*(d+1);
  return rsize * (x + n*y);
}

__device__ int patbase(int n, int l, int x, int y){
  return l * (x + n*y);
}

__device__ void init_pattern(int* pat, int maxl){
  pat[0] = 1;
  for (int i=1; i<maxl; ++i){
    pat[i] = 0;
  }
}

__device__ int next_pattern(int* pat, int modes, int maxl){

  // find last valid index in pattern
  int last = 0;
  while (last < maxl && pat[last] > 0){ ++last; }
  --last;
  
  int i = last;
  while (i > 0 && pat[i] == modes){
    pat[i] = 1;
    --i;
  }
  if (i == 0){
    if (last == maxl - 1){
      
      // this was the last pattern
      return 0;
    } else {
      pat[last+1] = 1;
      return 1;
    }
  } else {
    pat[i] += 1;
    return 1;
  }
}


// turns a zonotope into a box by giving intervals for each dim
__device__ void box(int d, float* z, float* b){
  float v[2];
  v[0] = 0.0f; v[1] = 0.0f;

  for (int i=0; i<d; ++i){
    for (int j=0; j<d; ++j){
      v[i] += abs(z[(d+1)*i+1+j]);
    }
  }
  
  b[0] = z[0] - v[0];
  b[1] = z[0] + v[0];
  b[2] = z[3] - v[1];
  b[3] = z[3] + v[1];
}


// tests if z1 is included in z2, z2 being a box
__device__ bool included_in(int d, float* z1, float* z2){
  float b1[4];
  float b2[4];
  box(d, z1, b1);
  box(d, z2, b2);

  if (b1[0] < b2[0]) {
    return false;
  }
  if (b1[1] > b2[1]){
    return false;
  }
  if (b1[2] < b2[2]) {
    return false;
  }
  if (b1[3] > b2[3]){
    return false;
  }
  
  return true;
}


// performs affine mapping on z (Ax+B)
__device__ void map(int d, float* z, float* a, float* b, float* r){
  for (int i=0; i<d; ++i){
    for (int j=0; j<=d; ++j){
      r[(d+1)*i+j] = 0.0f;
    }
  }
  
  for (int i=0; i<d; ++i){
    for (int j=0; j<d; ++j){
      r[(d+1)*i] += a[d*i+j] * z[(d+1)*i];
    }
    r[(d+1)*i] += b[i];
  }

  for (int i=0; i<d; ++i){
    for (int j=0; j<d; ++j){
      for (int k=0; k<d; ++k){
	r[(d+1)*i+j+1] += a[d*i+k] * z[(d+1)*k+j+1];	
      }
    }
  }
}



// ======================================================================

// main kernel function

// d  : dimension (FIXME: works only for d=2)
// z  : original zonotope, pre-scaled to grid
// a  : matrix for affine trans (FIXME: works only for 1 mode)
// b  : vector for affine trans (FIXME: works only for 1 mode)
// r  : zonotope to test inclusion
// inv: result matrix
// pat: result pattern matrix
// n  : grid size
// l  : max length of pattern

__global__ void affine_grid_kernel(int d, float* z, float* a, float *b, float *r, int* inv, float* res, int* pat, int n, int m, int l)
{
  int x = threadIdx.x;
  int y = threadIdx.y;

  // build grid cell by moving the center according to our indices (x,y)
  float g[6];
  g[0] = z[0] + 2.0f * x * (z[1] + z[2]);
  g[1] = z[1];
  g[2] = z[2];
  g[3] = z[d+1] + 2.0f * y * (z[d+2] + z[d+3]); 
  g[4] = z[4];
  g[5] = z[5];

  int pb = patbase(n, l, x, y);
  int success = 0;
  init_pattern(pat + pb, l);
  float cell[6];
  do {

    // initialize cell with grid
    for (int i=0; i<6; ++i){
      cell[i] = g[i];
    }

    // perform affine transformation according to pattern
    float tmp[6];
    for (int pos=0; pos<l; ++pos){
      if (pat[pb+pos] == 0) break;
      int mode = pat[pb+pos];
      float* amode = a + (mode-1)*d*d;
      float* bmode = b + (mode-1)*d;
      map(d, cell, amode, bmode, tmp);
      for (int i=0; i<6; ++i){
	cell[i] = tmp[i];
      }
    }
    
    // check inclusion in invariant region
    if (included_in(d, cell, r)){
    
      // that's all, folks
      success = 1;
      break;
    }
  } while (next_pattern(pat + pb, m, l));		   

  // store result 
  inv[x+n*y] = success;

  // store result image
  int rb = rbase(n, d, x, y);
  for (int i=0; i<d*(d+1); ++i){
    res[rb+i] = cell[i];
  }
}


// ======================================================================

/* Public entry point.
 * d  : dimension (FIXME: only 2 works at this time)
 * z  : original zonotope 
 * a  : matrix of affine trans.
 * b  : vector of affine trans.
 * r  : invariant region
 * inv: matrix of ints (bools) for result
 * res: resulting images (for debugging only, should be removed later)
 * n  : grid size (number of cells per dimension)
 */
int affine_grid_trans(int d, float* z, float* a, float *b, float* r, int* inv, float* res, int* pat, int n, int m, int l)
{
  int size = sizeof( float );

  // device copies 
  float *d_z, *d_a, *d_b, *d_r, *d_res;
  int   *d_inv, *d_pat;

  int z_size = size * d * (d+1);
  int r_size = z_size;
  int a_size = size * m * d * d;
  int b_size = size * m * d;
  int res_size = size * d * (d+1) * n * n;
  int pat_size = sizeof( int ) * l * n * n;
  int inv_size = sizeof( int ) * n * n;

  // prepare grid: Here, we create a copy of the original
  // zonotope. Then, its center is moved to the center of the first
  // grid cell (0,0). The generators are scaled by 1/n, so they can be
  // used in each cell without further computation
  //
  // +---+---+---+   ^ Gx
  // |   |   |   |   |         Gx'
  // +---+---+---+   |         ^
  // |   | C |   |   |    Gy   |
  // +---+---+---+   +------>  +--> Gy'
  // | C'|   |   |
  // +---+---+---+
  //
  float zg[d*(d+1)];
  
  // scale generators
  float scale = 1.0 / float(n);
  for (int i=0; i<d; ++i){
    for (int j=0; j<d; ++j){
      zg[(d+1)*i+j+1] = scale*z[(d+1)*i+j+1];
    }
  }

  // move center to bottom left grid cell center
  for (int i=0; i<d; ++i){
    zg[(d+1)*i] = z[(d+1)*i];
    for (int j=0; j<d; ++j){
      zg[(d+1)*i] -= float(n-1) * zg[(d+1)*i+j+1];
    }
  }
  

  // allocate memory on GPU
  cudaMalloc( (void **) &d_z, z_size );
  cudaMalloc( (void **) &d_a, a_size );
  cudaMalloc( (void **) &d_b, b_size );
  cudaMalloc( (void **) &d_r, r_size );
  cudaMalloc( (void **) &d_inv, inv_size );
  cudaMalloc( (void **) &d_res, res_size );
  cudaMalloc( (void **) &d_pat, pat_size );

  // copy inputs to device
  cudaMemcpy( d_z, zg, z_size, cudaMemcpyHostToDevice );
  cudaMemcpy( d_a, a, a_size, cudaMemcpyHostToDevice );
  cudaMemcpy( d_b, b, b_size, cudaMemcpyHostToDevice );
  cudaMemcpy( d_r, z, r_size, cudaMemcpyHostToDevice );

  // Kernel invocation with one block of N * N * 1 threads. Each
  // thread will be able to identify its position on the grid by its
  // thread indices.
  int numBlocks = 1;
  dim3 threadsPerBlock(n, n); 

  // launch the kernel on the GPU
  affine_grid_kernel<<< numBlocks,  threadsPerBlock >>>( d, d_z, d_a, d_b, d_r, d_inv, d_res, d_pat, n, m, l);
 
  // copy results back to host
  cudaMemcpy( inv, d_inv, inv_size, cudaMemcpyDeviceToHost );
  cudaMemcpy( res, d_res, res_size, cudaMemcpyDeviceToHost );
  cudaMemcpy( pat, d_pat, pat_size, cudaMemcpyDeviceToHost );

  // clean up
  cudaFree( d_z );
  cudaFree( d_a );
  cudaFree( d_b );
  cudaFree( d_r );
  cudaFree( d_inv );
	
  return 0;
} /* end main */
