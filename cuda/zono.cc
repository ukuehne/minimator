#include <octave/oct.h>

DEFUN_DLD (zono, args, ,
           "Test zonotope c interface")
{
  int nargin = args.length ();

  if (nargin != 1)
    print_usage ();
  else {
    NDArray Z = args(0).array_value ();
    if (! error_state){
      octave_idx_type cols = Z.cols();
      octave_idx_type rows = Z.rows();
      for (octave_idx_type i = 0; i < rows; ++i){
	for (octave_idx_type j = 0; j < cols; ++j){
	  float e = Z.elem(i,j);
	  octave_stdout << e << "\n";
	}
      }
    }
  }


  // int status = vector_add();
  // int status = 0;
  // octave_stdout << "exit status " << status << "\n";

  return octave_value_list ();
}
