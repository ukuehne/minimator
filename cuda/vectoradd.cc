#include <octave/oct.h>
#include "vectoradd.h"

DEFUN_DLD (vectoradd, args, nargout,
           "Hello World Help String")
{
  int nargin = args.length ();

  octave_stdout << "vectoradd has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

  int status = vector_add();
  // int status = 0;

  octave_stdout << "exit status " << status << "\n";

  return octave_value_list ();
}
