open Gmp
open Gmp.Z.Infixes
open Ppl_ocaml
open Array

type rat = Gmp.Q.t
type vec = rat array
type mtx = vec array

type zvec = Gmp.Z.t array
type zmtx = zvec array

type affine_map = linear_expression list * Gmp.Z.t


let string_of_vec v =
	Array.fold_left (fun str a -> str ^ (Gmp.Q.to_string a) ^ " ") "" v
	
let string_of_mtx m =
	Array.fold_left (fun str row -> str ^ (string_of_vec row) ^ "\n") "" m


let normalize m v =
	let collect v =
		Array.fold_left (fun l a ->
			a :: l
		) [] v in
	let mcollect m =
		Array.fold_left (fun l row ->
			l @ (collect row)
		) [] m in
	let coeffs = (collect v) @ (mcollect m) in
	let lcm = List.fold_left (fun lcm q ->
		let d = Gmp.Q.get_den q in
		Gmp.Z.lcm lcm d
	) Gmp.Z.one coeffs in
	let norm v =
		Array.map (fun q ->
			let q' = Gmp.Q.mul q (Gmp.Q.from_z lcm) in
			let n, d = Gmp.Q.get_num q', Gmp.Q.get_den q' in
			assert (Gmp.Z.equal Gmp.Z.one d);
			n
		) v in
	let v' = norm v in
	let m' = Array.map (norm) m in
	(* print_string ("lcm: " ^ (Gmp.Z.to_string lcm) ^ "\n"); *)
	(m', v', lcm)
	

let add_indices =	Array.mapi (fun i x -> (i,x))

let make_linexp row c =
	let row' = add_indices row in
	Array.fold_left (fun e (i,a) ->
		Plus (e, Times (a, Variable i))
	) (Coefficient c) row' 

let zip f a1 a2 =
	List.map2 f (Array.to_list a1) (Array.to_list a2)
	
let make m v =
	let m', v', z = normalize m v in
	((zip make_linexp m' v'), z)	

let apply p (les, z) =
	let q = ppl_new_C_Polyhedron_from_C_Polyhedron p in
	let i = ref 0 in
	List.iter (fun e ->
		ppl_Polyhedron_affine_image q !i e z;
		i := !i + 1
	) les;
	q
		