open Gmp
open Ppl_ocaml
open Array

type rat = Gmp.Q.t
type vec = rat array
type mtx = vec array
type affine_map 


val string_of_mtx: mtx -> string

val make: mtx	-> vec -> affine_map
val apply: polyhedron -> affine_map -> polyhedron