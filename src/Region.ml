open Ppl_ocaml
open AffineMap
open Global

type region = {
	v : rational_box;
	f : affine_map;	
	e : vec option;
	n : int;
}

type decomp = region list

let bbox p =
	ppl_new_Rational_Box_from_C_Polyhedron p	
	
let make_poly region =
	ppl_new_C_Polyhedron_from_Rational_Box region.v


let rec term_phase t i =
	match t with
	| Variable j -> if i=j then 1 else 0
  | Coefficient z -> 0
  | Unary_Plus t' -> term_phase t' i
  | Unary_Minus t' -> - (term_phase t' i)
  | Plus (t1, t2) -> (term_phase t1 i) + (term_phase t2 i)
  | Minus (t1, t2) -> (term_phase t1 i) - (term_phase t2 i)
  | Times (z, t') -> (Gmp.Z.sgn z) * (term_phase t' i)


let phase c i =
	match c with
	| Equal _ -> 0
	| Less_Than (t1, t2)
	| Less_Or_Equal (t1, t2) -> -(term_phase t1 i) + (term_phase t2 i)
	| Greater_Than (t1, t2)
	| Greater_Or_Equal	(t1, t2) -> (term_phase t1 i) - (term_phase t2 i)
		

				
	
let rec string_of_term t =	
	match t with
	| Variable i -> "x" ^ (string_of_int i)
  | Coefficient z -> Gmp.Z.to_string z
  | Unary_Plus t' -> string_of_term t'
  | Unary_Minus t' -> "-" ^ (string_of_term t')
  | Plus (t1, t2) -> (string_of_term t1) ^ " + " ^ (string_of_term t2)
  | Minus (t1, t2) -> (string_of_term t1) ^ " - (" ^ (string_of_term t2) ^ ")"
  | Times (z, t') -> (Gmp.Z.to_string z) ^ "*" ^ (string_of_term t')
	
let string_of_constraint c =
	match c with
	| Equal (t1, t2) -> (string_of_term t1) ^ " = " ^ (string_of_term t2) 
	| Less_Than (t1, t2) -> (string_of_term t1) ^ " < " ^ (string_of_term t2) 
	| Less_Or_Equal (t1, t2) -> (string_of_term t1) ^ " <= " ^ (string_of_term t2) 
	| Greater_Than (t1, t2) -> (string_of_term t1) ^ " > " ^ (string_of_term t2) 
	| Greater_Or_Equal (t1, t2) -> (string_of_term t1) ^ " >= " ^ (string_of_term t2) 

				
let print_constraints p =
	let cs = ppl_Polyhedron_get_minimized_constraints p in
	 List.iter (fun c ->
		print_string ((string_of_constraint c) ^ "\n")
	 ) cs		
		
let rec evaluate_term f t =
	match t with
		| Coefficient c -> c
		| Variable v -> (
			  try f v 
			  with _ -> begin
					print_error ("No value was found for variable " 
						^ (string_of_int v) ^ ", while trying to evaluate a linear term.");
					exit 1
				end)
		| Plus (lterm, rterm) -> ( 
				let lval = evaluate_term f rterm in
				let rval = evaluate_term f lterm in
				Gmp.Z.add lval rval)
		| Minus (lterm, rterm) -> (
				let lval = evaluate_term f rterm in
				let rval = evaluate_term f lterm in
				Gmp.Z.sub lval rval)
		| Times (fac, rterm) -> ( 
				let rval = evaluate_term f rterm in
				Gmp.Z.mul fac rval)
		| Unary_Plus t' -> evaluate_term f t'
		| Unary_Minus t' -> begin
				let x = evaluate_term f t' in
				Gmp.Z.neg x
			end
		
		

				
		
let unit_vector i =
 	fun j -> if i = j then Gmp.Z.one else Gmp.Z.zero		

		
				
let sigma c =
	match c with
	| Greater_Or_Equal (t1, t2) -> begin
			let x = evaluate_term (unit_vector 0) t1 
			and y = evaluate_term (unit_vector 1) t1
			and z = evaluate_term (fun _ -> Gmp.Z.zero) t2 in
			(x, y, z)
		end
	| _ -> begin
			print_error "non-standard constraint.";
			exit 1
		end
							
let make_constraint (x, y, z) =
	let tx = Times (x, Variable 0)
	and ty = Times (y, Variable 1) in
	Greater_Or_Equal (Plus (tx,ty),	Coefficient z)										



let point_of_generator = function
	| Closure_Point (expr, c) 
	| Point (expr, c) ->  
			let x, y = 
				(evaluate_term (unit_vector 0) expr,
				 evaluate_term (unit_vector 1) expr) in
			let q = Gmp.Q.from_z c in
			let xf = Gmp.Q.to_float (Gmp.Q.div (Gmp.Q.from_z x) q) in
			let yf = Gmp.Q.to_float (Gmp.Q.div (Gmp.Q.from_z y) q) in
			(xf, yf)
	| _ -> begin
			print_error "No point";
			exit 1
		end
	
	
let points_of_poly p =
	let generators = ppl_Polyhedron_get_minimized_generators p in
	let points = List.map point_of_generator generators in
	points
			
				
let print_poly p =
	let points = points_of_poly p in
	print_int (List.length points);
	print_string " Points:\n";
	let print_point (x,y) =
		print_string ("(" ^ (string_of_float x)
			^ ", " ^ (string_of_float y)
			^ ")\n") in
	List.iter print_point points;
	()


(* cosine of the angle between the x-axis and the *)
(* vector from (x0, y0) to (x,y)                  *)			
let cosine (x0, y0) (x, y) =
	if x0 = x && y0 = y then 1. else
		let vx = x -. x0 in
		let vy = y -. y0 in
		let len = sqrt (vx *. vx +. vy *. vy) in
		vx /. len 
					
(* compare two points wrt. their angle to the x-axis *)					
let cosine_compare p0 p1 p2 =
	let cmp = (cosine p0 p2) -. (cosine p0 p1) in
	if cmp > 0. then 1 else
		if cmp < 0. then -1 else 0

type alignment_type = 
	| Left_turn
	| Right_turn
	| Collinear

let alignment (x1, y1) (x2, y2) (x3, y3) =
	let cmp = (x2 -. x1) *. (y3 -. y1) -. (y2 -. y1) *. (x3 -. x1) in
	if cmp > 0. then Left_turn else
		if cmp < 0. then Right_turn else Collinear
	
(* returns true if p1 is closer to p0 than p2 to p0 *)
let dist_leq (x0, y0) (x1, y1) (x2, y2) =
	let d1 = (x1 -. x0)*.(x1 -. x0) +. (y1 -. y0)*.(y1 -. y0) in
	let d2 = (x2 -. x0)*.(x2 -. x0) +. (y2 -. y0)*.(y2 -. y0) in
	d1 <= d2


let list_only_once l =
	List.fold_left (fun l' x ->
		if (List.mem x l') then l' else x :: l' 
	) [] l


(* build the convex hull for a list of points *)
let graham_hull points =
	(* remove duplicate points *)
	let points = list_only_once points in
	if (2 > List.length points) then
		points
	else begin
  	(* find the lowest leftmost point and remove it from the list *)
  	let p0 = List.fold_left (fun (x0, y0) (x, y) -> 
  		if y < y0 || (y = y0 && x < x0) then (x, y) else (x0, y0)
  	) (List.hd points) (List.tl points)	in
  	(* sort the list wrt. to the angle to p0 *)
  	let points = List.sort (cosine_compare p0) (List.filter ((<>) p0) points) in
  	(* move pivot point to head of the list *)
  	let points =  p0 :: points in
  	(* iterate on points, discard points that induce a right-turn *)
  	let rec make_hull p1 p2 hull rest =
  		if rest = [] then
  			p2 :: p1 :: hull
  		else (
  			let p3 = List.hd rest in
  			match alignment p1 p2 p3 with
  				| Left_turn  ->
  						(* store first point, move on *) 
  						make_hull p2 p3 (p1 :: hull) (List.tl rest)				
  				| Right_turn ->
  						(* discard middle point, go on or backtrack *) 
  						if hull = [] then 
  							make_hull p1 p3 hull (List.tl rest)
  						else
  							(* backtrack to detect long "inward bended" point sequences *)
  							make_hull (List.hd hull) p1 (List.tl hull) rest
  				| Collinear  ->
  					(* discard closest point, go on *) 
  					if dist_leq p1 p2 p3 then 
  						make_hull p1 p3 hull (List.tl rest)
  					else
  						make_hull p1 p2 hull (List.tl rest)
  		)	in		 
  	(* close polygon *)
  	let points = List.append points [List.hd points] in
  	(* build hull *)
  	match points with
  		| p1 :: p2 :: rest ->
  			(* first two points must be on hull *)
  			make_hull p1 p2 [] rest
  		| _ -> points
	end

let plot_poly p =
	let points = graham_hull (points_of_poly p) in
	List.fold_left (fun s (px, py) -> 
		s ^ (string_of_float px) ^ " " ^ (string_of_float py) ^ "\n"
	) "" points

		
let plot_polys filename polys =
	let outfile = try (open_out filename) with
		| Sys_error e -> print_string ("The file " ^ filename ^ " could not be opened.\n" ^ e); exit 0; 
	in		
	List.iter (fun p ->
		let str = plot_poly p in
		output_string outfile (str ^ "\n")
	) polys;
	close_out outfile
	
	
let plot_decomp filename decomp =
	let outfile = try (open_out filename) with
		| Sys_error e -> print_string ("The file " ^ filename ^ " could not be opened.\n" ^ e); exit 0; 
	in		
	let boxes = List.map make_poly decomp in
	List.iter (fun p ->
		let str = plot_poly p in
		output_string outfile (str ^ "\n")
	) boxes;
	close_out outfile


let make_box bounds =
	let n = List.length bounds in
	let box = ppl_new_Rational_Box_from_space_dimension n Universe in
	let d = ref 0 in
	List.iter (fun (l, u) ->
		let lp, lq = Gmp.Q.get_num l, Gmp.Q.get_den l
		and up, uq = Gmp.Q.get_num u, Gmp.Q.get_den u in
		ppl_Rational_Box_add_constraint box 
			(Greater_Or_Equal (Times (lq, Variable !d), Coefficient lp));
		ppl_Rational_Box_add_constraint box 
			(Less_Or_Equal    (Times (uq, Variable !d), Coefficient up));	
		d := !d + 1;
	) bounds;
	box


let make_region box m ev i = {
	v = box;
	f = m;
	e = ev;
	n = i;
	}


let bounding_box polys =
	if polys = [] then 
		ppl_new_Rational_Box_from_space_dimension 2 Empty 
	else begin
		let boxes = List.map bbox polys in
		let bbox = List.hd boxes in
		List.iter (ppl_Rational_Box_upper_bound_assign bbox) (List.tl boxes);
		bbox
	end
	
let convex_hull polys =
	if polys = [] then
		ppl_new_C_Polyhedron_from_space_dimension 2 Empty 
	else begin
		let hull = List.hd polys in 
		List.iter (ppl_Polyhedron_upper_bound_assign hull) (List.tl polys);
		hull
	end


let lower_bound_of_box box i =
	let bounded, p, q, _ =
		ppl_Rational_Box_minimize box (Variable i) in
		assert bounded;
		Gmp.Q.from_zs p q
		
let upper_bound_of_box box i =
	let bounded, p, q, _ =
		ppl_Rational_Box_maximize box (Variable i) in
		assert bounded;
		Gmp.Q.from_zs p q
		

let bounds bbox =
	let n = ppl_Rational_Box_space_dimension bbox in
	let rec collect i = 
		if i = 0 then [] else (i-1) :: collect (i-1) in
	let dims = List.rev (collect n) in
	let lower_bound i = lower_bound_of_box bbox i in
	let upper_bound i = upper_bound_of_box bbox i in
	List.map (fun i -> (lower_bound i, upper_bound i)) dims
	
	
let overapprox_bounds bounds =
	let underaprx q =
		let f = Gmp.FR.from_q q in
		Gmp.Q.from_float (Gmp.FR.to_float_mode ~mode:Gmp.GMP_RNDD f) in
	let overaprx q =
		let f = Gmp.FR.from_q q in
		Gmp.Q.from_float (Gmp.FR.to_float_mode ~mode:Gmp.GMP_RNDU f) in
	List.map (fun (l,u) -> (underaprx l, overaprx u)) bounds	
	
	

(* convert a decomposition into a list of polyhedra *)
let init decomp =
	List.map make_poly decomp


let split_poly decomp poly =
	let boxes = List.map make_poly decomp in
	let cuts = List.map (fun box ->
		let q = ppl_new_C_Polyhedron_from_C_Polyhedron box in
		ppl_Polyhedron_intersection_assign q poly;
		q
	) boxes in
	List.filter (fun p -> not (ppl_Polyhedron_is_empty p)) cuts


(* let split_poly decomp poly =                                     *)
(* 	let cuts poly box =                                            *)
(* 		not (ppl_Polyhedron_is_disjoint_from_Polyhedron box poly) in *)
(*   let boxes = List.map make_poly decomp in                       *)
(* 	let cut_regions = List.filter (cuts poly) boxes in             *)
(* 	List.map (fun box ->                                           *)
(* 		let q = ppl_new_C_Polyhedron_from_C_Polyhedron box in        *)
(* 		ppl_Polyhedron_intersection_assign q poly;                   *)
(* 		q                                                            *)
(* 	) cut_regions                                                  *)


let split_polys decomp polys =
	List.fold_left (fun imgs poly ->
		let tiles = split_poly decomp poly in
		imgs @ tiles
	)	[] polys

let compact polys =
	let dominated ps q =
		List.exists (fun p ->
			ppl_Polyhedron_contains_Polyhedron p q
		) ps in
	let comp = 
		List.fold_left (fun cps q ->
		  if (dominated cps q) then cps else q :: cps
	  ) [] in
	comp (comp polys)


		
		
let partition dec polys =
	let boxes = List.map make_poly dec in
	let partitions, rest = List.fold_left	(fun (ps, rest) box ->
		let pin, pout = List.partition (ppl_Polyhedron_contains_Polyhedron box) rest in
		pin :: ps, pout
	) ([], polys) boxes in
	if not (rest = []) then begin
		print_error "could not partition polyhedra";
		exit 1
	end;
	partitions

		
let index_partition dec polys =
	let boxes = List.map (fun r -> (r, make_poly r)) dec in
	let partitions, rest = List.fold_left	(fun (ps, rest) (i, box) ->
		let pin, pout = List.partition (ppl_Polyhedron_contains_Polyhedron box) rest in
		(i, pin) :: ps, pout
	) ([], polys) boxes in
	if not (rest = []) then begin
		print_error "could not partition polyhedra";
		exit 1
	end;
	partitions
						
		
(* simplify image by constructing the bounding boxes for each region *)
let simplify dec polys =
	let ps = partition dec polys in
	let boxes = List.map bounding_box ps in
	List.map ppl_new_C_Polyhedron_from_Rational_Box boxes
	

let simplify_trunc dec polys =
	let ps = partition dec polys in
	let boxes = List.map bounding_box ps in
	let grow box =
		if ppl_Rational_Box_is_empty box then
			ppl_new_C_Polyhedron_from_Rational_Box box
		else begin
			let bds = bounds box in
			let bds' = overapprox_bounds bds in
			let box' = make_box bds' in
			ppl_new_C_Polyhedron_from_Rational_Box box'
		end in
	List.map grow boxes
	
		
(* simplify image by constructing the convex hull for each region *)
let hull dec polys =
	let ps = partition dec polys in
	List.map convex_hull ps
		


open Gmp.Q.Infixes

let enlarge_constraint ex ey c =
	let (x, y, z) = sigma c in
	let z' = Gmp.Q.from_z z in
	let z'' = z' -/ (Gmp.Q.abs (Gmp.Q.from_z x)) */ ex in
	let z''' = z'' -/ (Gmp.Q.abs (Gmp.Q.from_z y)) */ ey in
	let d = Gmp.Q.get_den z''' in
	make_constraint (Gmp.Z.mul d x, Gmp.Z.mul d y, Gmp.Q.get_num z''')


let enlarge_poly ex ey p =
	let cs = ppl_Polyhedron_get_minimized_constraints p in
	let cs' = List.map (enlarge_constraint ex ey) cs in
	ppl_new_C_Polyhedron_from_constraints cs'

	
let enlarge es ps = 
	let ex = es.(0) in
	let ey = es.(1) in
	List.map (enlarge_poly ex ey) ps



let bloat_poly es p =
	let box = ppl_new_C_Polyhedron_from_Rational_Box(
		ppl_new_Rational_Box_from_C_Polyhedron p) in
	let p' = enlarge_poly es.(0) es.(1) p in
	let box' = enlarge_poly es.(0) es.(1) box in
	ppl_Polyhedron_intersection_assign p' box';
	p'


let bloat es ps =		
	let boxes = List.map (fun p ->
		let box = ppl_new_Rational_Box_from_C_Polyhedron p in
		ppl_new_C_Polyhedron_from_Rational_Box box
	) ps in
	let ps' = enlarge es ps in
	let boxes' = enlarge es boxes in
	List.map2 (fun p b ->
			ppl_Polyhedron_intersection_assign p b; p
	) ps' boxes'


(** compute the post delta images of a set of polys wrt. a decomposition *)
let post_delta_imgs decomp polys =
	let enclosing_region p =
		let bbox = bbox p in 
		try begin
			List.find (fun r ->
				ppl_Rational_Box_contains_Rational_Box r.v bbox
			) decomp
		end	with Not_found -> begin
			print_error "Precondition violation: no enclosing region for poly found.";
			exit 1
		end in
	let imgs = List.map (fun p ->
		let r = enclosing_region p in
		let img = AffineMap.apply p r.f in
		match r.e with
		| None -> img
		| Some es -> bloat_poly es img
	) polys in
	imgs


let test_separation dec polys =
	let boxes = List.map make_poly dec in
	List.for_all (fun p -> 
		List.exists (fun box ->
			ppl_Polyhedron_strictly_contains_Polyhedron box p
		) boxes
	) polys 


let img r p =
	let im = AffineMap.apply p r.f in
	match r.e with
	| None -> im
	| Some es -> bloat_poly es im


let make_graph dec polys =
	let parts = index_partition dec polys in
	let gr = ref (Graph.create_graph ()) in
	let nodes = ref [] in
	List.iter (fun (r, ps) ->
		List.iter (fun p ->
			let node = Graph.create_node p in
			nodes := (r, node) :: !nodes;
			gr := Graph.add_node !gr node r.n
		) ps 
	) parts;
	List.iter (fun (r, (i, p)) ->
		let q = img r p in
		let succs = List.filter (fun (_, (_, s)) ->
			ppl_Polyhedron_contains_Polyhedron s q
		) !nodes in
		List.iter (fun (_, (j,_)) ->
			gr := Graph.add_edge !gr (i,j) 
		) succs;
	) !nodes;
	!gr

(* compute the post of a list of polys wrt. a decomposition.*)
(* PRE : polys must not intersect region boundaries         *)
(* POST: polys do not intersect region boundaries           *)
let post_delta decomp polys =
	let imgs = post_delta_imgs decomp polys in
	let imgs' = split_polys decomp imgs in
	let sep = if ((List.length imgs) = (List.length imgs')) then
		test_separation decomp imgs'
	else
		false 
	in
	if sep then begin
		let gr = make_graph decomp imgs' in
		Graph.dump gr "limit";
		let gr' = Graph.reduce gr in
		Graph.dump gr' "cycles"
	end;
	sep, compact imgs'


let verify_inv dec =
	let make_powerset ps =
		let pow = ppl_new_Pointset_Powerset_C_Polyhedron_from_C_Polyhedron (List.hd ps) in
		List.iter (fun p ->
			ppl_Pointset_Powerset_C_Polyhedron_add_disjunct pow p
		) (List.tl ps); pow  in
	let ps = init dec in
	let ps' = post_delta_imgs dec ps in
	let pow = make_powerset ps 
	and pow' = make_powerset ps' in
	ppl_Pointset_Powerset_C_Polyhedron_geometrically_covers_Pointset_Powerset_C_Polyhedron pow pow'



	