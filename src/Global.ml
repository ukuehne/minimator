let path = ref "."

let print_error str =
	prerr_endline ("[ERR] " ^ str)

let print_info str =
	prerr_endline ("[INF] " ^ str)
	
let set_path p = path := p

let get_path _ = !path
	