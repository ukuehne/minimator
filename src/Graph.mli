open AffineMap
open Ppl_ocaml

type node_t = int * polyhedron
type t

val create_graph: unit -> t
val create_node : polyhedron -> node_t

val add_node: t -> node_t -> int -> t
val add_edge: t -> int * int -> t

val reduce: t -> t

val dump: t -> string -> unit