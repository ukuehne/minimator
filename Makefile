TARGET  = Minimator
BUILD   = ocamlbuild -no-hygiene
CFLAGS  = -annot,-I,+gmp,-I,/usr/local/lib/ppl
STATIC  = -cclib,-static,-cclib,-lppl,-cclib,-lppl_ocaml,-cclib,-lstdc++,-cclib,-lgmp,-cclib,-lgmpxx
DYNAMIC = -cclib,-lppl
LFLAGS  = -I,+gmp,-I,/usr/local/lib/ppl,$(STATIC)

LIBS    = gmp,ppl_ocaml,unix

EXEDIR  = bin
EXE     = minimator


default all: $(TARGET).native annotate install

%.byte: FORCE
	$(BUILD) $@ -cflags $(CFLAGS) -libs $(LIBS) -lflags $(LFLAGS)

%.native: FORCE
	$(BUILD) $@ -cflags $(CFLAGS) -libs $(LIBS) -lflags $(LFLAGS)


annotate: $(TARGET).native
	cp _build/src/*.annot src

install: $(TARGET).native
	test -d $(EXEDIR) || mkdir $(EXEDIR)
	cp $(TARGET).native $(EXEDIR)/$(EXE)

clean:
	$(BUILD) -clean
	rm -f src/*.annot
	rm -f $(EXEDIR)/$(EXE)

FORCE:
